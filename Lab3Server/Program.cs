﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace Lab3Server {
    internal class Program {
        private static FileStorageEmulator _emulator;
        private static void Main(string[] args) {
            Console.WriteLine("Введите колво файлов на сервере");
            var filesCnt = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите максимальное кол-во обрабатываемых запросов");
            var threadsCount = int.Parse(Console.ReadLine());
            ThreadPool.SetMaxThreads(threadsCount, 10);
            _emulator = new FileStorageEmulator(filesCnt);
            _emulator.RequestStateChanged += (sender, message) => Console.WriteLine(message); 
            Start();
        }
        private static void Start() {
            var ipHost = Dns.GetHostEntry("localhost");
            var ipAddr = ipHost.AddressList[0];
            var ipEndPoint = new IPEndPoint(ipAddr, 23456);
            var listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(ipEndPoint);
            listener.Listen(100);
            Console.WriteLine("Сервер запущен");
            while (true) {
                var handler = listener.Accept();
                _emulator.AcceptRequest(handler);
            }
        }
    }
}