﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Lab3Classes;

namespace Lab3Server {
    public class FileStorageEmulator {
        public ConcurrentDictionary<string, FileFlags> Files { get; }
        private static readonly Random Rnd;
        public event EventHandler<string> RequestStateChanged = delegate { };
        static FileStorageEmulator() {
            Rnd = new Random(Guid.NewGuid().GetHashCode());
        }
        public FileStorageEmulator(int filesCnt) {
            Files = new ConcurrentDictionary<string, FileFlags>();
            for (var i = 0; i < filesCnt; ++i) {
                Files.TryAdd($"Файл{i}", new FileFlags(0, 0, 0));
            }
        }

        public void AcceptRequest(Socket socket) {
            try {
                var type = new byte[4];
                socket.Receive(type);
                var reqType = (RequestTypes) BitConverter.ToInt32(type, 0);
                if (reqType != RequestTypes.Write) {
                    var files = BinarySerializer<string[]>.Serialize(Files.Keys.ToArray());
                    var len = BitConverter.GetBytes(files.Length);
                    socket.Send(len);
                    socket.Send(files);
                }
                var fileNameSize = new byte[4];
                socket.Receive(fileNameSize);
                var byteFileName = new byte[BitConverter.ToInt32(fileNameSize, 0)];
                socket.Receive(byteFileName);
                var fileName = Encoding.Unicode.GetString(byteFileName);
                ThreadPool.QueueUserWorkItem(HandleRequest, new RequestParams(reqType, fileName, socket));
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                socket.Close();
            }
        }
        private void HandleRequest(object reqParams) {
            var sw = new Stopwatch();
            sw.Start();
            var parameters = (RequestParams)reqParams;
            var fileExists = Files.ContainsKey(parameters.FileName);
            Thread.CurrentThread.Name = parameters.ToString();
            var flags = new FileFlags(0, 0, 0);
            if (fileExists) {
                while (!Files.TryGetValue(parameters.FileName, out flags)) {

                }
            }
            if (parameters.ReqType == RequestTypes.Write) {
                RequestStateChanged.Invoke(this, Thread.CurrentThread.Name + "Проверка существования такого же файла");
                Thread.Sleep(200);
                if (fileExists) {
                    if (Rnd.NextDouble() < 0.2) {
                        RequestStateChanged.Invoke(this, Thread.CurrentThread.Name + "отмена операции");
                        return;
                    }
                }
            }
            if (fileExists) {
                RequestStateChanged.Invoke(this,
                    Thread.CurrentThread.Name +
                    $": Проверка наличия блокировки на {GetCheckLockType(parameters.ReqType)}");
            }
            WaitAvailableAndIncFileFlags(parameters, Thread.CurrentThread.Name, flags, fileExists);
            var reqTime = Rnd.Next(parameters.TimeInterval.Key, parameters.TimeInterval.Value);
            Thread.Sleep(reqTime * 1000);
            DecFileFlagsAndFinishHandle(parameters, Thread.CurrentThread.Name, flags, sw);
        }
        private string GetCheckLockType(RequestTypes type) {
            switch (type) {
                case RequestTypes.Read:
                    return "чтение";
                case RequestTypes.Write:
                    return "удаление";
                case RequestTypes.Rewrite:
                    return "запись";
                default:
                    return "";
            }
        }
        private static string GetLockTypes(RequestTypes type) =>
            type == RequestTypes.Read ? @"запись и удаление." : @"чтение, запись и удаление.";
        private void WaitAvailableAndIncFileFlags(RequestParams parameters, string descr, FileFlags flags, bool exists) {
            var iters = 0;
            while (true) {
                lock (flags.SyncRoot) {
                    if (!IsAvailableFile(parameters.ReqType, flags)) {
                        if (iters == 0) {
                            RequestStateChanged.Invoke(this, descr + $": Ожидание снятия блокировки на " +
                                                                     GetCheckLockType(parameters.ReqType));
                        }
                    } else {
                        RequestStateChanged.Invoke(this, descr + ": Увеличение счетчиков блокировки на " +
                                                                      $"{GetLockTypes(parameters.ReqType)}");
                        if (parameters.ReqType != RequestTypes.Read) {
                            flags.LockedToReadingCounter += 1;
                        }
                        flags.LockedToDelitingCounter += 1;
                        flags.LockedToWritingCounter += 1;
                        if (parameters.ReqType == RequestTypes.Write) {
                            if (exists) {
                                RequestStateChanged.Invoke(this, Thread.CurrentThread.Name + " Удаление файла");
                                Thread.Sleep(200);
                                while (!Files.TryRemove(parameters.FileName, out _)) {
                                }
                            }
                            RequestStateChanged.Invoke(this, Thread.CurrentThread.Name + " Создание файла");
                            Thread.Sleep(200);
                            while (!Files.TryAdd(parameters.FileName, flags)) {
                                
                            }
                        }
                        break;
                    }
                    ++iters;
                }
            }
        }
        private void DecFileFlagsAndFinishHandle(RequestParams parameters, string descr, FileFlags flags, Stopwatch sw) {
            lock (flags.SyncRoot) {
                RequestStateChanged.Invoke(this, descr + ": Уменьшение счетчиков блокировки на " +
                                                            $"{GetLockTypes(parameters.ReqType)}");
                if (parameters.ReqType != RequestTypes.Read) {
                    flags.LockedToReadingCounter -= 1;
                }
                flags.LockedToDelitingCounter -= 1;
                flags.LockedToWritingCounter -= 1;
                sw.Stop();
                var msg = Thread.CurrentThread.Name + ": Запрос обработан "
                          + $"Вермя: {sw.ElapsedMilliseconds / 1000.0} сек.";
                try {
                    parameters.ClientSosket.Send(Encoding.Unicode.GetBytes(msg));
                    RequestStateChanged.Invoke(this, msg);
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                finally {
                    parameters.ClientSosket.Close();
                }
            }
        }
        private bool IsAvailableFile(RequestTypes type, FileFlags flags) {
            switch (type) {
                case RequestTypes.Read:
                    return flags.LockedToReadingCounter == 0;
                case RequestTypes.Write:
                    return flags.LockedToDelitingCounter == 0;
                case RequestTypes.Rewrite:
                    return flags.LockedToWritingCounter == 0;
                default:
                    return false;
            }
        }
    }
}