import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.BinaryConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import java.util.Collections;
import java.util.Scanner;

public class Server {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        IgniteConfiguration cfg = new IgniteConfiguration();
        BinaryConfiguration binCfg = new BinaryConfiguration();
        binCfg.setClassNames(Collections.singletonList(ComputeFunc.class.getName()));
        cfg.setBinaryConfiguration(binCfg);
        try(Ignite ignite = Ignition.start(cfg)){
            sc.next();
        }
    }
}