import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.BinaryConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import java.math.BigInteger;
import java.util.*;
public class Client {
    private static Random Rnd;
    static {
        Rnd = new Random();
    }
    public static void main(String[] args) {
        int rows = 3000;
        int columns = 3000;
        int[][] matrix = new int[rows][columns];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < columns; ++j) {
                matrix[i][j] = Rnd.nextInt(9) + 1;
            }
        }
        Scanner sc = new Scanner(System.in);
        IgniteConfiguration cfg = new IgniteConfiguration();
        BinaryConfiguration binCfg = new BinaryConfiguration();
        binCfg.setClassNames(Collections.singletonList(ComputeFunc.class.getName()));
        cfg.setBinaryConfiguration(binCfg);
        cfg.setClientMode(true);
        List<int[]> rowCollection = new ArrayList<>(Arrays.asList(matrix));
        long startTime, endTime;
        try(Ignite ignite = Ignition.start(cfg)) {
            startTime = System.nanoTime();
            BigInteger sum = BigInteger.ZERO;
            Collection<BigInteger> res = ignite.compute().apply(new ComputeFunc(), rowCollection);
            for (BigInteger b : res) {
                sum = sum.add(b);
            }
            endTime = System.nanoTime();
            System.out.println("Сумма произведений элементов строк = " + sum);
            System.out.println("Время работы " + (endTime - startTime) / 1e9 + " секунд.");
            sc.next();
        }
    }
}