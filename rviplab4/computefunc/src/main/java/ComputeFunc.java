﻿import org.apache.ignite.lang.IgniteClosure;
import java.math.BigInteger;
import java.util.Arrays;
public class ComputeFunc implements IgniteClosure<int[], BigInteger> {
    @Override
    public BigInteger apply(int[] arg) {
        BigInteger res = BigInteger.ONE;
        for (int elem : arg) {
            res = res.multiply(BigInteger.valueOf(elem));
        }
        return res;
    }
}