﻿namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.logRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.maxRequesrtsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.requestIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.countFilesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.maxRequesrtsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestIntervalNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countFilesNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // logRichTextBox
            // 
            this.logRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logRichTextBox.Location = new System.Drawing.Point(274, 30);
            this.logRichTextBox.Name = "logRichTextBox";
            this.logRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.logRichTextBox.Size = new System.Drawing.Size(456, 438);
            this.logRichTextBox.TabIndex = 0;
            this.logRichTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(413, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Лог обработки запросов";
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startButton.Location = new System.Drawing.Point(12, 404);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(110, 54);
            this.startButton.TabIndex = 4;
            this.startButton.Text = "Старт";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopButton.Location = new System.Drawing.Point(146, 402);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(110, 56);
            this.stopButton.TabIndex = 5;
            this.stopButton.Text = "Остановить генерацию запросов";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // maxRequesrtsNumericUpDown
            // 
            this.maxRequesrtsNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxRequesrtsNumericUpDown.Location = new System.Drawing.Point(12, 78);
            this.maxRequesrtsNumericUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.maxRequesrtsNumericUpDown.Name = "maxRequesrtsNumericUpDown";
            this.maxRequesrtsNumericUpDown.Size = new System.Drawing.Size(227, 22);
            this.maxRequesrtsNumericUpDown.TabIndex = 1;
            this.maxRequesrtsNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // requestIntervalNumericUpDown
            // 
            this.requestIntervalNumericUpDown.DecimalPlaces = 1;
            this.requestIntervalNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.requestIntervalNumericUpDown.Location = new System.Drawing.Point(12, 161);
            this.requestIntervalNumericUpDown.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.requestIntervalNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.requestIntervalNumericUpDown.Name = "requestIntervalNumericUpDown";
            this.requestIntervalNumericUpDown.Size = new System.Drawing.Size(227, 22);
            this.requestIntervalNumericUpDown.TabIndex = 2;
            this.requestIntervalNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // countFilesNumericUpDown
            // 
            this.countFilesNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countFilesNumericUpDown.Location = new System.Drawing.Point(12, 242);
            this.countFilesNumericUpDown.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.countFilesNumericUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.countFilesNumericUpDown.Name = "countFilesNumericUpDown";
            this.countFilesNumericUpDown.Size = new System.Drawing.Size(228, 22);
            this.countFilesNumericUpDown.TabIndex = 6;
            this.countFilesNumericUpDown.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(9, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(230, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Обрабат. одновр. запросов(макс.)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(9, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Частота появления запросов (сек)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(228, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Изначально файлов в хранилище";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 470);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.countFilesNumericUpDown);
            this.Controls.Add(this.requestIntervalNumericUpDown);
            this.Controls.Add(this.maxRequesrtsNumericUpDown);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.logRichTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "РВИП. Лабораторная работа №2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.maxRequesrtsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestIntervalNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countFilesNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox logRichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.NumericUpDown maxRequesrtsNumericUpDown;
        private System.Windows.Forms.NumericUpDown requestIntervalNumericUpDown;
        private System.Windows.Forms.NumericUpDown countFilesNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

