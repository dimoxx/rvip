﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Timers;

namespace Lab2 {
    public partial class Form1 : Form {
        private FileStorageEmulator _emulator;
        private System.Timers.Timer _createRequestTimer;
        private static readonly Random Rnd;
        static Form1() {
            Rnd = new Random();
        }
        public Form1() {
            InitializeComponent();
        }
        private void StartButton_Click(object sender, EventArgs e) {
            ThreadPool.GetMaxThreads(out var maxWorkerThreads, out _);
            ThreadPool.GetAvailableThreads(out var availableWorkerThreads, out _);
            if (maxWorkerThreads != availableWorkerThreads) {
                MessageBox.Show($@"Дождитесь завершения обработки {maxWorkerThreads - availableWorkerThreads} запросов");
                return;
            }
            logRichTextBox.Text = "";
            ThreadPool.SetMaxThreads((int)maxRequesrtsNumericUpDown.Value, 5);
            _emulator = new FileStorageEmulator((int)countFilesNumericUpDown.Value);
            _emulator.RequestStateChanged += Emulator_RequestStateChanged;
            _createRequestTimer = new System.Timers.Timer {
                Interval = (int)(1000 * requestIntervalNumericUpDown.Value),
            };
            _createRequestTimer.Elapsed += CreateRequest;
            _createRequestTimer.Start();
        }
        private void CreateRequest(object sender, ElapsedEventArgs e) {
            var cntRequests = Rnd.Next(1, 15);
            for (var i = 0; i < cntRequests; ++i) {
                var files = _emulator.Files.Keys.ToList();
                var index = Rnd.Next(0, files.Count);
                var reqType = (RequestTypes)Enum.ToObject(typeof(RequestTypes), (byte)Rnd.Next(1, 4));
                var fileName = files[index];
                if (reqType == RequestTypes.Write) {
                    if (Rnd.NextDouble() < 0.4) {
                        fileName = $"Файл{Rnd.Next(files.Count + 10 * i, files.Count + 30 * i)}";
                    }
                }
                _emulator.AcceptRequest(new RequestParams(reqType, fileName));
            }
        }
        private void Emulator_RequestStateChanged(object sender, string state) {
            BeginInvoke((MethodInvoker)(() => {
                var sb = new StringBuilder(logRichTextBox.Text);
                sb.Append(state + Environment.NewLine);
                logRichTextBox.Text = sb.ToString();
            }));
        }
        private void StopButton_Click(object sender, EventArgs e) {
            _createRequestTimer.Stop();
            MessageBox.Show(@"Генерация запросов Отсановлена");
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            Environment.Exit(0);
        }
    }
}