﻿namespace Lab2 {
    public class FileFlags {
        public int LockedToReadingCounter { get; set; }
        public int LockedToWritingCounter { get; set; }
        public int LockedToDelitingCounter { get; set; }
        public object SyncRoot { get; }
        public FileFlags(int lockedToReadingCounter, int lockedToWritingCounter, int lockedToDelitingCounter) {
            LockedToReadingCounter = lockedToReadingCounter;
            LockedToWritingCounter = lockedToWritingCounter;
            LockedToDelitingCounter = lockedToDelitingCounter;
            SyncRoot = new object();
        }
    }
}