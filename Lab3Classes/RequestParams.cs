﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace Lab3Classes {
    [Serializable]
    public class RequestParams {
        public RequestTypes ReqType { get; set; }
        public string FileName { get; set; }
        public KeyValuePair<int, int> TimeInterval { get; set; }
        public Socket ClientSosket { get; set; }
        public RequestParams(RequestTypes type, string fileName, Socket sosket) {
            ReqType = type;
            FileName = fileName;
            ClientSosket = sosket;
            switch (type) {
                case RequestTypes.Read:
                    TimeInterval = new KeyValuePair<int, int>(5, 7);
                    break;
                case RequestTypes.Write:
                    TimeInterval = new KeyValuePair<int, int>(7, 10);
                    break;
                case RequestTypes.Rewrite:
                    TimeInterval = new KeyValuePair<int, int>(2, 6);
                    break;
                default:
                    TimeInterval = new KeyValuePair<int, int>(0, 0);
                    break;
            }
        }
        public override string ToString() {
            return $"{Thread.CurrentThread.ManagedThreadId}, {FileName}, {ReqType.CustomToString()}";
        }
    }
}