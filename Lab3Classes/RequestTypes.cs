﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Classes {
    public enum RequestTypes : byte {
        Read = 1,
        Write = 2,
        Rewrite,
    }
    public static class RequestTypeExtensions {
        public static string CustomToString(this RequestTypes type) {
            switch (type) {
                case RequestTypes.Read:
                    return "Чтение";
                case RequestTypes.Write:
                    return "Запись";
                case RequestTypes.Rewrite:
                    return "Перезапись";
                default:
                    return "Неопознанный запрос";
            }
        }
    }
}
