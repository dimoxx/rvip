﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Lab3Classes;
namespace Lab3Client {
    internal static class Program {
        private static readonly int Port;
        private static readonly Random Rnd;
        static Program() {
            Port = 23456;
            Rnd = new Random(Guid.NewGuid().GetHashCode());
        }
        private static void Main(string[] args) {
            while (true) {
                SendRequest();
                Thread.Sleep(6000);
            }
        }
        private static void SendRequest() {
            var infoOnFilesSize = new byte[4];
            var files = new string[] { };
            var ipHost = Dns.GetHostEntry("localhost");
            var ipAddr = ipHost.AddressList[0];
            var ipEndPoint = new IPEndPoint(ipAddr, Port);
            var sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try {
                sender.Connect(ipEndPoint);
                var type = (RequestTypes) Rnd.Next(1, 4);
                sender.Send(BitConverter.GetBytes((int) type));
                if (type != RequestTypes.Write) {
                    sender.Receive(infoOnFilesSize);
                    var size = BitConverter.ToInt32(infoOnFilesSize, 0);
                    var data = new byte[size];
                    sender.Receive(data);
                    files = BinarySerializer<string[]>.Deserialize(data);
                    Console.WriteLine("Получен список файлов");
                }
                var filename = string.Empty;
                if (type == RequestTypes.Write) {
                    filename = $@"Файл {Rnd.Next(10000)}";
                }
                else {
                    var fileInd = Rnd.Next(0, files.Length);
                    filename = files[fileInd];
                }
                var bytesFileName = Encoding.Unicode.GetBytes(filename);
                sender.Send(BitConverter.GetBytes(bytesFileName.Length));
                sender.Send(bytesFileName);
                Console.WriteLine(@"Отправлен запрос: " + filename + " - " + type.CustomToString());
                Console.WriteLine(@"Ожидание ответа...");
                var answer = new byte[2048];
                var len = sender.Receive(answer);
                var str = Encoding.Unicode.GetString(answer, 0, len);
                sender.Close();
                Console.WriteLine(str);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                sender.Close();
            }
        }
    }
}